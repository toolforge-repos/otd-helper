// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use anyhow::Result;
use chrono::{Datelike, Duration, NaiveDate, Utc};
use mysql_async::Pool;
use rocket::response::Redirect;
use rocket::State;
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::Serialize;

mod query;
mod sql;

const DAY_FORMAT: &str = "%B %-d";

// Route `/?<day>` to `/day/<day>`
// and `/` to `/day/{tomorrow}`
#[get("/?<day>")]
fn index(day: Option<&str>) -> Redirect {
    let uri = match day {
        Some(day) => format!("/day/{day}"),
        None => {
            let day = (Utc::now() + Duration::try_days(1).unwrap())
                .format(DAY_FORMAT);
            format!("/day/{day}")
        }
    };
    Redirect::to(uri.replace(' ', "_"))
}

#[derive(Serialize)]
struct DayTemplate {
    day: String,
    title: String,
    infos_by_section: Vec<(String, Vec<query::PageInfo>)>,
    date_nav: Option<(String, String)>,
}

#[derive(Serialize)]
struct ErrorTemplate {
    error: String,
}

#[get("/day/<day>")]
async fn day(pool: &State<Pool>, day: &str) -> Result<Template, Template> {
    let day = day.replace('_', " ");
    let title = format!("Wikipedia:Selected anniversaries/{day}");
    match query::fetch_sa(pool, &title).await {
        Ok(infos_by_section) => {
            let mut infos_by_section: Vec<_> =
                infos_by_section.into_iter().collect();
            let date_nav = parse_date(&day);
            infos_by_section.reverse();
            Ok(Template::render(
                "day",
                DayTemplate {
                    day,
                    title,
                    infos_by_section,
                    date_nav,
                },
            ))
        }
        Err(err) => Err(Template::render(
            "error",
            ErrorTemplate {
                error: err.to_string(),
            },
        )),
    }
}

fn parse_date(day: &str) -> Option<(String, String)> {
    let year = Utc::now().year();
    let parsed = NaiveDate::parse_from_str(
        &format!("{day} {year}"),
        &format!("{} %Y", DAY_FORMAT),
    )
    .ok()?;
    let tomorrow = (parsed + Duration::try_days(1).unwrap()).format(DAY_FORMAT);
    let yesterday =
        (parsed - Duration::try_days(1).unwrap()).format(DAY_FORMAT);
    Some((yesterday.to_string(), tomorrow.to_string()))
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index, day])
        .manage(Pool::new(
            toolforge::connection_info!("enwiki")
                .unwrap()
                .to_string()
                .as_str(),
        ))
        .attach(Template::fairing())
        .attach(Healthz::fairing())
}
