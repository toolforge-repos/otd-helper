use anyhow::Result;
use mwseaql::sea_query::{expr::Expr, MysqlQueryBuilder, Query};
use mwseaql::{
    page_assessments::{PageAssessments, PageAssessmentsProjects},
    Page,
};
use mysql_async::prelude::Queryable;
use mysql_async::Pool;
use std::collections::{BTreeMap, BTreeSet};

pub(crate) async fn query(
    pool: &Pool,
    titles: &[String],
) -> Result<(
    BTreeMap<String, BTreeSet<String>>,
    BTreeMap<String, BTreeSet<String>>,
)> {
    let query = Query::select()
        .from(PageAssessments::Table)
        .column(Page::Title)
        .column(PageAssessmentsProjects::ProjectTitle)
        .column(PageAssessments::Class)
        .inner_join(
            Page::Table,
            Expr::col(Page::Id).equals(PageAssessments::PageId),
        )
        .inner_join(
            PageAssessmentsProjects::Table,
            Expr::col(PageAssessments::ProjectId)
                .equals(PageAssessmentsProjects::ProjectId),
        )
        .and_where(Expr::col(Page::Namespace).eq(0))
        .and_where(Expr::col(Page::Title).is_in(titles))
        .to_string(MysqlQueryBuilder);
    let mut conn = pool.get_conn().await?;
    println!("{query}");
    let results: Vec<(String, String, String)> = conn.query(query).await?;
    let mut projects: BTreeMap<String, BTreeSet<String>> = BTreeMap::new();
    let mut classes: BTreeMap<String, BTreeSet<String>> = BTreeMap::new();
    for (title, project, class) in results {
        let project = normalize_project(project);
        projects
            .entry(title.to_string())
            .and_modify(|x| {
                x.insert(project.to_string());
            })
            .or_insert_with(|| BTreeSet::from([project]));
        if !class.is_empty() {
            classes
                .entry(title)
                .and_modify(|x| {
                    x.insert(class.to_string());
                })
                .or_insert_with(|| BTreeSet::from([class]));
        }
    }
    Ok((projects, classes))
}

fn normalize_project(input: String) -> String {
    let (parent, suffix) = match input.split_once('/') {
        Some((parent, suffix)) => (parent, suffix),
        None => {
            return input;
        }
    };
    let ret = match suffix.strip_prefix("WikiProject ") {
        Some(project) => project,
        None => parent,
    };
    ret.to_string()
}

#[test]
fn test_normalize_project() {
    assert_eq!(normalize_project("Biography".to_string()), "Biography");
    assert_eq!(
        normalize_project("Africa/WikiProject Uganda".to_string()),
        "Uganda"
    );
    assert_eq!(
        normalize_project(
            "California/San Francisco Bay Area task force".to_string()
        ),
        "California"
    );
}
