// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use cached::proc_macro::cached;
use chrono::NaiveDate;
use indexmap::IndexMap;
use mwbot::parsoid::node::Section;
use mwbot::parsoid::WikinodeIterator;
use mwbot::{Bot, Page};
use mysql_async::Pool;
use percent_encoding::percent_decode_str;
use serde::Serialize;
use std::collections::{BTreeMap, BTreeSet};

const USER_AGENT: &str = toolforge::user_agent!("otd-helper");

// From https://en.wikipedia.org/wiki/Special:WhatLinksHere?target=Template%3AOn+this+day&namespace=&hidetrans=1&hidelinks=1
const OTD_TALK: [&str; 5] = [
    "Template:On this day",
    "Template:Selected anniversary",
    "Template:OTDtalk",
    "Template:SAtalk",
    "Template:OnThisDay",
];

const ARTICLE_HISTORY: [&str; 7] = [
    "Template:Article history",
    "T:AH",
    "Template:Articlehistory",
    "Template:Article milestones",
    "Template:Articlemilestones",
    "Template:Article History",
    "Template:ArticleHistory",
];

#[cached(sync_writes = true, result = true)]
async fn bot() -> Result<Bot> {
    Ok(Bot::builder(
        "https://en.wikipedia.org/w/api.php".to_string(),
        "https://en.wikipedia.org/api/rest_v1".to_string(),
    )
    .set_user_agent(USER_AGENT.to_string())
    .build()
    .await?)
}

#[derive(Debug, Serialize)]
pub(crate) struct PageInfo {
    pub(crate) title: String,
    pub(crate) dbkey: String,
    pub(crate) orange_tags: BTreeSet<String>,
    pub(crate) inline_tags: BTreeMap<String, usize>,
    pub(crate) appearances: BTreeSet<usize>,
    pub(crate) projects: Option<BTreeSet<String>>,
    pub(crate) classes: Option<BTreeSet<String>>,
}

async fn handle_page(bot: Bot, page: Page) -> Result<PageInfo> {
    let page2 = page.clone();
    // Parse the talk page in a separate thread, concurrently
    let bot2 = bot.clone();
    let talk_handle =
        tokio::spawn(async move { parse_talk_page(bot2, &page2).await });
    let html = page.html().await?;
    let mut orange_tags = BTreeSet::new();
    let mut inline_tags = BTreeMap::new();
    {
        let html = html.into_mutable();
        for tag in html.select(".ambox-content") {
            let classes = tag
                .as_element()
                // safe because selector verified it's an element
                .unwrap()
                .attributes
                .borrow()
                .get("class")
                // safe because selector already verified it has a class
                .unwrap()
                .to_string();
            for class in classes.split(' ') {
                if class.starts_with("box-") {
                    // safe: verified prefix above
                    let (_, suffix) = class.split_once('-').unwrap();
                    orange_tags.insert(suffix.replace('_', " "));
                }
            }
        }
        for sup in html.select("sup.Inline-Template") {
            inline_tags
                .entry(sup.text_contents())
                .and_modify(|x| *x += 1)
                .or_insert(1);
        }
    }

    let appearances = talk_handle.await??;
    Ok(PageInfo {
        title: page.title().to_string(),
        dbkey: page.as_title().dbkey().to_string(),
        orange_tags,
        inline_tags,
        appearances,
        projects: None,
        classes: None,
    })
}

async fn parse_talk_page(bot: Bot, page: &Page) -> Result<BTreeSet<usize>> {
    assert_eq!(page.namespace(), 0);
    let talk = bot.page(&format!("Talk:{}", page.title()))?;
    let html = talk.html().await?;
    let mut years = BTreeSet::new();

    {
        let html = html.into_mutable();
        for temp in html.filter_templates()? {
            if OTD_TALK.contains(&temp.name().as_str()) {
                for (name, value) in temp.params() {
                    if name.starts_with("date") {
                        if let Some(year) = date_to_year(&value) {
                            years.insert(year);
                        }
                    }
                }
            } else if ARTICLE_HISTORY.contains(&temp.name().as_str()) {
                for (name, value) in temp.params() {
                    if name.starts_with("otd") && name.ends_with("date") {
                        if let Some(year) = date_to_year(&value) {
                            years.insert(year);
                        }
                    }
                }
            }
        }
    }
    Ok(years)
}

/// "2016-01-01" to 2016, None on error
fn date_to_year(input: &str) -> Option<usize> {
    let (year, _) = input.split_once('-')?;
    year.parse().ok()
}

fn is_date(title: &str) -> bool {
    // We need a full YMD for this, so hack in a date and expect it to match
    NaiveDate::parse_from_str(
        &format!("2023 {title}"),
        &format!("%Y {}", crate::DAY_FORMAT),
    )
    .is_ok()
}

pub(crate) async fn fetch_sa(
    pool: &Pool,
    title: &str,
) -> Result<IndexMap<String, Vec<PageInfo>>> {
    let bot = bot().await?;
    let page = bot.page(title)?;
    let html = page.html().await?;
    // This page layout is a mess. Everything is in proper sections except
    // for the actual page we care about!
    // So...we iterate over the sections, remove them, then
    // do one more final pass over section 0 which contains the actual links.
    let mut links_by_section: IndexMap<String, Vec<String>> = IndexMap::new();
    {
        let html = html.into_mutable();
        for section in html.iter_sections() {
            let heading = match section.heading() {
                Some(heading) => heading.text_contents(),
                None => {
                    continue;
                }
            };
            let links = parse_section(&section);
            if !links.is_empty() {
                links_by_section.insert(heading, links);
            }
            section.detach();
        }
        links_by_section.insert(
            "Planned".to_string(),
            parse_section(&html.iter_sections()[0]),
        );
    }

    let mut titles = vec![];
    let mut handles_by_section: IndexMap<String, Vec<_>> = IndexMap::new();
    for (section, links) in links_by_section {
        for link in links {
            let page = bot.page(&link)?;
            if page.namespace() != 0 {
                continue;
            }
            titles.push(page.as_title().dbkey().to_string());
            let bot2 = bot.clone();
            handles_by_section
                .entry(section.to_string())
                .or_default()
                .push(tokio::spawn(
                    async move { handle_page(bot2, page).await },
                ));
        }
    }
    let (mut projects, mut classes) = crate::sql::query(pool, &titles).await?;
    let mut infos: IndexMap<String, Vec<_>> = IndexMap::new();
    for (section, handles) in handles_by_section {
        for handle in handles {
            let mut info = handle.await??;
            info.projects = projects.remove(&info.dbkey);
            info.classes = classes.remove(&info.dbkey);
            infos.entry(section.to_string()).or_default().push(info);
        }
    }
    Ok(infos)
}

fn parse_section(section: &Section) -> Vec<String> {
    let mut ret = vec![];
    for tag in section.select("b a") {
        if let Some(link) = tag.as_wikilink() {
            let target = percent_decode_str(&link.target())
                .decode_utf8()
                // unwrap: Shouldn't be getting invalid UTF-8 here
                .unwrap()
                .to_string();
            if target == "List of days of the year" || is_date(&target) {
                continue;
            }
            //dbg!((page.title(), is_date(page.title())));
            ret.push(target);
        }
    }
    dbg!(&ret);
    ret
}

#[test]
fn test_is_date() {
    assert!(is_date("November 19"));
    assert!(!is_date("What"));
}
